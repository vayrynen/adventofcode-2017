/*
 * Ideas: few.
 */



  //          //
 /* Includes */
//          //
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

  //            //
 /* Prototypes */
//            //
void parse_args(int, char *[]);
void usage(void);
int get_judge_count(int);

  //           //
 /* Constants */
//           //

// #define DEBUG

const char * const PROGRAM_NAME = "generators";

const long FACTOR_A = 16807;
const long FACTOR_B = 48271;

const int MOD_BY = 2147483647;

const int ROUNDS = 5000000;

const int SEED_A = 618;
const int SEED_B = 814;
const int DEBUG_SEED_A = 65;
const int DEBUG_SEED_B = 8921;

int main(int argc, char *argv[]) {
    parse_args(argc, argv);

    int judge_count = get_judge_count(ROUNDS);
    printf("Judge count: %d\n", judge_count);

    return 0;
}

int get_judge_count(int num_rounds) {
    int judge_count = 0;

#ifdef DEBUG
    long a = DEBUG_SEED_A;
    long b = DEBUG_SEED_B;
#else
    long a = SEED_A;
    long b = SEED_B;
#endif // DEBUG

    time_t start_time = clock();

    for(int i=0; i<num_rounds; i++) {
        do {
            a = (a * FACTOR_A) % MOD_BY;
        }
        while(a % 4 != 0);
        do {
            b = (b * FACTOR_B) % MOD_BY;
        }
        while(b % 8 != 0);

        if((short)a == (short)b) {
            judge_count++;
        }
        if(i%1000000 == 0) {
            float time = (float) (clock() - start_time) / CLOCKS_PER_SEC;
            printf("Finished %d rounds in %fs.\n", i, time);
        }
    }
    return judge_count;
}

void parse_args(int argc, char *argv[]) {
    if(argc != 1) {
        usage();
    }
}

void usage() {
    printf("Usage: %s\n", PROGRAM_NAME);
    exit(1);
}
