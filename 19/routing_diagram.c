/*
 * Idea: When moving, we know the direction we're moving in. The following
 *       are what characters we may find along the way:
 *
 *       1) A pointer in the same direction. (- or |). Just keep moving.
 *       2) An alpha char. Jot it down and keep moving.
 *       3) A direction change marker (+). We cannot turn back, so there
 *          are three directions to check for a valid one. A valid one
 *          will contain either of cases 1) and 2). Note that the new
 *          direction must match the pointer type, so that for example when
 *          moving "upward" or "downward", it must be of type "|".
 *       4) We move out of the array (we have exited and are done).
 *
 * The coordinate system used has its origo in the top left.
 *
 */


  //          //
 /* Includes */
//          //
#define _GNU_SOURCE
#define DEBUG 0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

  //         //
 /* Structs */
//         //
typedef struct st_dir {
    int delta_x, delta_y;
    char *str;
} Direction;

  //           //
 /* Constants */
//           //
const int NUM_LINES = 200;
const int LINE_LENGTH = 202 + 1;

const Direction UP = {0, -1, "up"};
const Direction RIGHT = {1, 0, "right"};
const Direction DOWN = {0, 1, "down"};
const Direction LEFT = {-1, 0, "left"};

  //            //
 /* Prototypes */
//            //
void parse_args(int, char *[], FILE**);
void usage(char*);
int read_input(FILE*, char [][LINE_LENGTH]);
void travel(char [][LINE_LENGTH], char*, int*);
void get_start_pos_and_dir(char [][LINE_LENGTH], int*, int*, Direction*);
Direction turn(char [][LINE_LENGTH], int, int, Direction);

  //           //
 /* Functions */
//           //
int main(int argc, char *argv[]) {
    FILE *file_handle = NULL;
    parse_args(argc, argv, &file_handle);

    char lines[NUM_LINES][LINE_LENGTH];
    read_input(file_handle, lines);

    char result_str[128];
    result_str[127] = '\0';

    // Initialize with the first step
    int num_steps = 1;

    travel(lines, result_str, &num_steps);

    printf("Result after %d steps: %s\n", num_steps, result_str);

    return 0;
}

void travel(char lines[][LINE_LENGTH], char *result, int *num_steps) {
    int x, y;
    Direction dir;
    get_start_pos_and_dir(lines, &x, &y, &dir);

    if(DEBUG) {
        printf("Starting at (%d,%d) with movement vector (%d,%d).\n",
                x, y, dir.delta_x, dir.delta_y);
    }

    char symbol;
    while(1) {
        x += dir.delta_x;
        y += dir.delta_y;

        if(x < 0 || x >= LINE_LENGTH || y < 0 || y >= NUM_LINES) {
            if(DEBUG) {
                printf("OOB at %d %d!\n", x, y);
            }
            break;
        }

        symbol = lines[y][x];
        if(symbol == '|') {
            if(dir.delta_y != 0) {
                if(DEBUG) {
                    printf("Regular vertical move.\n");
                }
            }
            else {
                if(DEBUG) {
                    printf("Horizontal jump.\n");
                }
            }
        }
        else if(symbol == '-') {
            if(dir.delta_x != 0) {
                if(DEBUG) {
                    printf("Regular horizontal move.\n");
                }
            }
            else {
                if(DEBUG) {
                    printf("Vertical jump.\n");
                }
            }
        }
        else if(isalpha(symbol)) {
            if(DEBUG) {
                printf("Alpha symbol '%c' found.\n", symbol);
            }
            *(result++) = symbol;
        }
        else if(symbol == '+') {
            if(DEBUG) {
                printf("Crossing, uh oh...\n");
            }
            dir = turn(lines, x, y, dir);
            if(DEBUG) {
                printf("Turned %s.\n", dir.str);
            }
        }
        else {
            if(DEBUG) {
                printf("Unknown move... ending.\n");
            }
            break;
        }

        *num_steps = *num_steps + 1;
    }
}

Direction turn(char lines[][LINE_LENGTH], int x, int y, Direction currdir) {
    Direction directions[] = { UP, RIGHT, DOWN, LEFT };
    for(int i=0; i<4; i++) {
        Direction direction = directions[i];

        // U-turn, invalid
        if((currdir.delta_x == direction.delta_x && currdir.delta_y != direction.delta_y) ||
            (currdir.delta_x != direction.delta_x && currdir.delta_y == direction.delta_y)) {
            continue;
        }

        int new_x = x + direction.delta_x;
        int new_y = y + direction.delta_y;

        // Out of bounds, invalid
        if(new_x < 0 || new_x >= LINE_LENGTH || new_y < 0 || new_y >= NUM_LINES) {
            continue;
        }

        char next_symbol = lines[new_y][new_x];

        // Alpha's always okay
        if(isalpha(next_symbol)) {
            return direction;
        }
        else if(next_symbol == '|') {
            if(DEBUG) {
                printf("Trying |\n");
            }
            // Ok if we're moving vertically
            if(direction.delta_y != 0) {
                return direction;
            }
        }
        else if(next_symbol == '-') {
            if(DEBUG) {
                printf("Trying -\n");
            }
            // Ok if we're moving horizontally
            if(direction.delta_x != 0) {
                return direction;
            }
        }
    }

    fprintf(stderr, "Failed to find valid new direction... ooops!\n");
    exit(1);
}

// We assume that we always start at the top and move downward...
// so just scan the first line for a vertical marker (pipe) to find our xpos
// and set movement to Down.
void get_start_pos_and_dir(char lines[][LINE_LENGTH], int *x, int *y, Direction *dir) {
    *y = 0;
    *dir = DOWN;

    for(int i=0; i<LINE_LENGTH; i++) {
        if(lines[0][i] == '|') {
            *x = i;
            return;
        }
    }

    fprintf(stderr, "Failed to find start coordinates... ooops!\n");
    exit(1);
}

int read_input(FILE *file_handle, char lines[][LINE_LENGTH]) {
    char *linebuf = malloc(sizeof(char) * LINE_LENGTH);
    size_t read;

    int line_index = 0;

    while(getline(&linebuf, &read, file_handle) != -1) {
        strcpy(lines[line_index++], linebuf);
    }

    free(linebuf);
    fclose(file_handle);

    return -1;
}

void parse_args(int argc, char *argv[], FILE **file_handle) {
    if(argc != 2) {
        usage(argv[0]);
    }

    if((*file_handle = fopen(argv[1], "r")) == 0) {
        perror("Failed to open input file");
        exit(1);
    }
}

void usage(char *program_name) {
    printf("Usage: %s inputfile\n", program_name);
    exit(1);
}
