#!/usr/bin/env python

import os
import sys
import logging

def main():
    input_file = parse_args(sys.argv)
    data = read_bits(input_file)
    groups_found = 0

    while(any([ any(row) for row in data])):
        group_coords = find_group(data)
        for x, y in group_coords:
            data[x][y] = False
        groups_found = groups_found + 1

    print("Groups found: {}".format(groups_found))

def find_group(data):
    x, y = find_first_element(data)
    group_coords = [(x,y)]
    while True:
        valid_neighbours = find_valid_neighbours_for_group(data, group_coords)
        if len(valid_neighbours) == 0:
            break;
        else:
            group_coords = group_coords + valid_neighbours
    return group_coords

def find_valid_neighbours_for_group(data, group_coords):
    trial_set = set()

    for group_x, group_y in group_coords:
        for neighbour_x, neighbour_y in find_neighbour_coordinates(group_x, group_y):
            if data[neighbour_x][neighbour_y]:
                if (neighbour_x, neighbour_y) not in group_coords:
                    trial_set.add((neighbour_x, neighbour_y))

    return list(trial_set)

def find_neighbour_coordinates(x, y):
    neighbours = []
    for i, j in [ (x+1, y), (x-1, y), (x, y+1), (x, y-1) ]:
        if i >= 0 and i <= 127 and j >= 0 and j <= 127:
            neighbours.append((i,j))
    return neighbours

def find_first_element(data):
    for i in xrange(len(data)):
        for j in xrange(len(data[i])):
            if data[i][j]:
                return i, j

def read_bits(input_file):
    content = None
    with open(input_file, 'r') as ifd:
        content = ifd.readlines()

    return [ [ char == '1' for char in line.rstrip('\n') ] for line in content ]

def parse_args(argv):
    if len(argv) != 2:
        print_usage()
    input_file = argv[1]
    if not os.path.exists(input_file):
        logging.error("File not found: {}".format(input_file))
        sys.exit(1)
    return argv[1]

def print_usage():
    basename = os.path.basename(__file__)
    print("Usage: {} inputfile".format(basename))
    sys.exit(1)

if __name__ == "__main__":
    main()
